## نرم‌افزار مرکز فروش  تلفن همراه

به کمک نرم‌افزار تلفن همراه ،فروشنده میتواند به راحتی امور مربوط به خرید و  فروش  خود را مدیریت کند.


## ویژگی های کلیدی نرم‌افزار
* جلوگیری از اتلاف وقت
* مدیریت اسان و افزایش رضایت مشتریان
* صرفه جویی در هزینه


## تحلیل و طراحی پروژه

* در ابتدا به بررسی چند عملکرد اصلی در نرم‌افزار در قالب سناریو عملکردفروشنده  پرداخته ایم. [سناریو ها](documentation/SCENARIO.md)

*  [نیازمندی های پروژه](documentation/REQUIREMENTS.md)

*  [usecase diagram](documentation/USECASE.md)

*  [sequence diagram](documentation/SEQUENCE.md)
  
*  [class diagram](documentation/CLASS.md)

* [activity diagram](documentation/ACTIVITY.md)
 
* [database diagram](documentation/images/Database Diagrams.PNG)


## فازهای توسعه پروژه
1. صفحه ورود فروشنده
2. صفحه اصلی 
3. بخش مشتریان
4. بخش محصولات
5. بخش سفارشات
6. بخش محصولات فروخته شده


## توسعه دهندگان
|نام و نام خانوادگی|   ID     |
|:---------------|:---------------|
|     mozhdeh bagheri    | @mozhdeh.bagheri |
|     atefeh esmaeili     | @atefeh93 |  
