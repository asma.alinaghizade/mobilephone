using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;

namespace MOBILE
{
    public partial class products : Form
    {
        public products()
        {
            InitializeComponent();
        }


        private void products_Load(object sender, EventArgs e)
        {
            panel2.Hide();
            panel3.Hide();
            panel4.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.BackColor = Color.PowderBlue;
            button3.BackColor = Color.White;
            button5.BackColor = Color.White;

            panel2.Show();
            panel3.Show();
            button7.Show();
            button8.Show();
            textBox4.Show();
            button15.Hide();
            button16.Hide();
            button13.Hide();
            button14.Hide();
            dataGridView1.Hide();
            panel4.Hide();
            textBox1.Text = null;
            textBox3.Text = null;
            textBox4.Text = null;
            textBox5.Text = null;
            textBox7.Text = null;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            button5.BackColor = Color.PowderBlue;
            button3.BackColor = Color.White;
            button1.BackColor = Color.White;

            panel2.Show();
            panel3.Show();
            panel4.Show();

            button15.Show();
            button16.Show();
            textBox4.Show();
            button13.Hide();
            button14.Hide();
            button7.Hide();
            button8.Hide();
            dataGridView1.Hide();
            textBox1.Text = null;
            textBox3.Text = null;
            textBox4.Text = null;
            textBox5.Text = null;
            textBox7.Text = null;
            // label9.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            button3.BackColor = Color.PowderBlue;
            button1.BackColor = Color.White;
            button5.BackColor = Color.White;

            panel2.Show();
            panel3.Show();
            panel4.Show();

            button15.Hide();
            button16.Hide();
            textBox4.Hide();
            button13.Show();
            button14.Show();
            dataGridView1.Show();
            button7.Hide();
            button8.Hide();
            label9.Show();
            textBox1.Text = null;
            textBox3.Text = null;
            textBox4.Text = null;
            textBox5.Text = null;
            textBox7.Text = null;

        }

        private void button9_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            main m = new main();
            m.Show();
            Dispose();
        }

        private void button7_Click(object sender, EventArgs e)
        {

          
                  if (textBox1.Text == string.Empty ||  textBox3.Text == string.Empty || textBox5.Text == string.Empty)
                  {

                          MessageBox.Show("لطفااصلاعات را وارد کنید", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                          return;

                  }
            
                    SqlConnection con = new SqlConnection("Data Source=DESKTOP-F5PUFPH\\MSSQLSERVER2;Initial Catalog = mobilephone; Integrated Security = True");
                    con.Open();
                    SqlCommand cmd = new SqlCommand("insert into products (price,name,seller_id,numbers) values( '"+ textBox3.Text + "' , '" + textBox1.Text + "' ,1 , '"+ textBox5.Text + "')" , con );
                    cmd.ExecuteNonQuery();
                    textBox4.Text = "محصول با موفقیت اضافه شد";
                    con.Close();
           
               
           
       }


        private void button15_Click(object sender, EventArgs e)
        {
            int i = 0;

            if (textBox7.Text == string.Empty)
            {

                MessageBox.Show("لطفا کد محصول را وارد کنید", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;

            }


            try
            {

                SqlConnection con = new SqlConnection("Data Source=DESKTOP-F5PUFPH\\MSSQLSERVER2;Initial Catalog = mobilephone; Integrated Security = True");

                SqlCommand com = new SqlCommand("SELECT COUNT(*) FROM products where  code='" + textBox7.Text + "' ", con);

                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                    i = (int)com.ExecuteScalar();
                }//end if



                if (i > 0)
                {
                    SqlCommand cmd = new SqlCommand("delete  from products where code='" + textBox7.Text + "' ", con);
                    cmd.ExecuteNonQuery();
                    textBox4.Text = "محصول حذف شد";
                }


                else
                    textBox4.Text = "محصول موردنظر پیدا نشد";

                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }


        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
           // SetVisibleCore(false);
        }

        private void button14_Click(object sender, EventArgs e)
        {
            

                SqlConnection con = new SqlConnection("Data Source=DESKTOP-F5PUFPH\\MSSQLSERVER2;Initial Catalog = mobilephone; Integrated Security = True");
                //con.Open();
                SqlDataAdapter sda = new SqlDataAdapter("select * from products  where code='" + textBox7.Text + "' ", con);
                //cmd.ExecuteNonQuery();
                DataTable dt = new DataTable();
                sda.Fill(dt);
            dataGridView1.DataSource = dt;
           
        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
