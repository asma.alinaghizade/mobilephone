using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;

namespace MOBILE
{
    public partial class orders : Form
    {
        public orders()
        {
            InitializeComponent();
        }

        private void orders_Load(object sender, EventArgs e)
        {
            panel2.Hide();
            panel3.Hide();
            panel4.Hide();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            panel2.Show();
            panel3.Show();
            button7.Show();
            button8.Show();
            textBox6.Show();
            textBox1.Show();
            label1.Show();
            button15.Hide();
            button16.Hide();
            button13.Hide();
            button14.Hide();
            dataGridView1.Hide();
            panel4.Hide();
            textBox1.Text = null;
            textBox4.Text = null;
            textBox5.Text = null;
            textBox6.Text = null;
            textBox7.Text = null;
          


        }

        private void button5_Click(object sender, EventArgs e)
        {
            panel2.Show();
            panel3.Show();
            panel4.Show();
           
            button15.Show();
            button16.Show();
            textBox6.Show();
            textBox1.Hide();
            label1.Hide();
            button13.Hide();
            button14.Hide();
            button7.Hide();
            button8.Hide();
            dataGridView1.Hide();
            label9.Show();
            textBox1.Text = null;
            textBox4.Text = null;
            textBox5.Text = null;
            textBox6.Text = null;
            textBox7.Text = null;

        }

        private void button3_Click(object sender, EventArgs e)
        {

            panel2.Show();
            panel3.Show();
            panel4.Show();
            
            button15.Hide();
            button16.Hide();
            textBox6.Hide();
            button13.Show();
            textBox1.Hide();
            label1.Hide();
            button14.Show();
            button7.Hide();
            button8.Hide();
            dataGridView1.Show();
            label9.Show();
            textBox1.Text = null;
            textBox4.Text = null;
            textBox5.Text = null;
            textBox6.Text = null;
            textBox7.Text = null;
        }

        private void button10_Click(object sender, EventArgs e)
        {
            main m = new MOBILE.main();
            m.Show();
            Dispose();
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button14_Click(object sender, EventArgs e)
        {

            SqlConnection con = new SqlConnection("Data Source=DESKTOP-F5PUFPH\\MSSQLSERVER2;Initial Catalog = mobilephone; Integrated Security = True");
            //con.Open();
            SqlDataAdapter sda = new SqlDataAdapter("select * from orders where order_id='" + textBox7.Text + "' ", con);
            //cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            sda.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button15_Click(object sender, EventArgs e)
        {

            int i = 0;

            if (textBox7.Text == string.Empty)
            {

                MessageBox.Show("لطفا کد سفارش را وارد کنید", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;

            }


            try
            {

                SqlConnection con = new SqlConnection("Data Source=DESKTOP-F5PUFPH\\MSSQLSERVER2;Initial Catalog = mobilephone; Integrated Security = True");

                SqlCommand com = new SqlCommand("SELECT COUNT(*) FROM orders where  order_id='" + textBox7.Text + "' ", con);

                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                    i = (int)com.ExecuteScalar();
                }//end if



                if (i > 0)
                {
                    SqlCommand cmd = new SqlCommand("delete  from orders where order_id='" + textBox7.Text + "' ", con);
                    cmd.ExecuteNonQuery();
                    textBox6.Text = "سفارش حذف شد";
                }


                else
                    textBox6.Text = "سفارش موردنظر پیدا نشد";

                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }


        }

        private void button7_Click(object sender, EventArgs e)
        {

            if ( textBox5.Text == string.Empty || textBox4.Text == string.Empty || textBox1.Text==string.Empty)
            {

                MessageBox.Show("لطفااصلاعات را وارد کنید", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;

            }

            SqlConnection con = new SqlConnection("Data Source=DESKTOP-F5PUFPH\\MSSQLSERVER2;Initial Catalog = mobilephone; Integrated Security = True");
            con.Open();
            SqlCommand cmd = new SqlCommand("insert into orders (sellerr_id,number_order,code_product,costumer_id) values( 1 , '" + textBox5.Text + "' , '" + textBox4.Text + "' , '" + textBox1.Text + "')", con);
            cmd.ExecuteNonQuery();
            textBox6.Text = "سفارش با موفقیت  به لیست سفارشات اضافه شد";
            con.Close();


        }

        private void button16_Click(object sender, EventArgs e)
        {

        }

        private void button13_Click(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {

        }
    }
}

